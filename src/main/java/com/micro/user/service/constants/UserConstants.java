package com.micro.user.service.constants;

public class UserConstants {
    public static final String USER = "user";
    public static final String DEPARTMENT = "department";
    public static final String ADDRESS = "address";
    public static final String ROLE = "role";
    public static final String IDENTIFICATION = "identification";
}

package com.micro.user.service.constants;

public class FieldErrorConstants {
    public static final String NOT_NULL ="notnull";
    public static final String NOT_BLANK ="notblank";
    public static final String NOT_EMPTY ="notempty";

}

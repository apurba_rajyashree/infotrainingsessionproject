package com.micro.user.service.repo;

import com.micro.user.service.model.Identification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IdentificationRepo extends JpaRepository<Identification, Integer> {
}

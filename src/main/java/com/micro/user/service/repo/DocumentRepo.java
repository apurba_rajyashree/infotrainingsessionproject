package com.micro.user.service.repo;

import com.micro.user.service.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepo extends JpaRepository {

    @Query(value = "select * from document where uuid=?1", nativeQuery = true)
    Document findByUuid(String uuid);

}

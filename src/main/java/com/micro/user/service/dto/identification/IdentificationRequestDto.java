package com.micro.user.service.dto.identification;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class IdentificationRequestDto {

    private Integer id;

    private String identificationDocumentType;

    private String identificationDocumentNumber;

    private String identificationDocumentIssuedFrom;

    private Date identificationDocumentIssuedDateAd;

    private String identificationDocumentIssuedDateBs;
}

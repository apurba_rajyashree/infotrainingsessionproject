package com.micro.user.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.micro.user.service.dto.address.AddressResponseDto;
import com.micro.user.service.dto.department.DepartmentResponseDto;
import com.micro.user.service.dto.identification.IdentificationResponseDto;
import com.micro.user.service.dto.role.RoleResponseDto;
import com.micro.user.service.model.User;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponseDto {


    private Integer id;

    private String nameEnglish;

    private String nameNepali;

    private String email;

    private String about;

    private DepartmentResponseDto departmentResponseDto;
    private List<AddressResponseDto> addressResponseDtos;
    private List<RoleResponseDto> roleResponseDtos;
    private IdentificationResponseDto identificationResponseDto;

    public UserResponseDto(User user) {
        this.id = user.getId();
        this.nameEnglish = user.getNameEnglish();
        this.nameNepali = user.getNameNepali();
        this.email = user.getEmail();
        this.about = user.getAbout();
        this.departmentResponseDto = new DepartmentResponseDto(user.getDepartment());
        this.identificationResponseDto = new IdentificationResponseDto(user.getIdentification());
        this.addressResponseDtos = new ArrayList<>(user.getAddressList().stream().map(AddressResponseDto::new).toList());
        this.roleResponseDtos = new ArrayList<>(user.getRoleList().stream().map(RoleResponseDto::new).toList());
    }
}

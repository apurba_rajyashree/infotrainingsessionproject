package com.micro.user.service.dto.role;

import com.micro.user.service.model.Role;
import jakarta.annotation.security.RolesAllowed;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleResponseDto {
    private Integer id;

    private String nameEnglish;

    private String nameNepali;

    private String roleType;

    public RoleResponseDto(Role role) {
        this.id = role.getId();
        this.nameEnglish = role.getNameEnglish();
        this.nameNepali = role.getNameNepali();
        this.roleType = role.getRoleType();
    }
}

package com.micro.user.service.dto.address;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.micro.user.service.enums.AddressType;
import com.micro.user.service.enums.AddressTypeSerializer;
import com.micro.user.service.model.Address;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressResponseDto {

    private Integer id;

    private String country;

    private String district;

    private String province;

    private String street;

    @JsonSerialize(using = AddressTypeSerializer.class, as = Enum.class)
    private AddressType addressType;

    private Integer userId;
    private String userNameEnglish;
    private String userNameNepali;

    public AddressResponseDto(Address address) {
        this.id = address.getId();
        this.country = address.getCountry();
        this.district = address.getDistrict();
        this.province = address.getProvince();
        this.street = address.getStreet();
        this.addressType = address.getAddressType();
        this.userId = address.getUser().getId();
        this.userNameEnglish = address.getUser().getNameEnglish();
        this.userNameNepali = address.getUser().getNameNepali();
    }
}

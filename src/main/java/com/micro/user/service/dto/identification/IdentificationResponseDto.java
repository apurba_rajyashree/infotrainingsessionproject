package com.micro.user.service.dto.identification;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.micro.user.service.model.Identification;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class IdentificationResponseDto {
    private Integer id;

    private String identificationDocumentType;

    private String identificationDocumentNumber;

    private String identificationDocumentIssuedFrom;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date identificationDocumentIssuedDateAd;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private String identificationDocumentIssuedDateBs;

    public IdentificationResponseDto(Identification identification) {
        this.id = identification.getId();
        this.identificationDocumentType = identification.getIdentificationDocumentType();
        this.identificationDocumentNumber = identification.getIdentificationDocumentNumber();
        this.identificationDocumentIssuedFrom = identification.getIdentificationDocumentIssuedFrom();
        this.identificationDocumentIssuedDateAd = identification.getIdentificationDocumentIssuedDateAd();
        this.identificationDocumentIssuedDateBs = identification.getIdentificationDocumentIssuedDateBs();
    }
}


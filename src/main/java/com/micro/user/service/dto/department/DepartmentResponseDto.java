package com.micro.user.service.dto.department;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.micro.user.service.model.Department;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DepartmentResponseDto {

    private Integer id;
    private String nameEnglish;
    private String nameNepali;
    private String description;

    public DepartmentResponseDto(Department department) {
        this.id = department.getId();
        this.nameEnglish = department.getNameEnglish();
        this.nameNepali = department.getNameNepali();
        this.description = department.getDescription();
    }
}

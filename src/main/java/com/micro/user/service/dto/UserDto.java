package com.micro.user.service.dto;

import com.micro.user.service.constants.FieldErrorConstants;
import com.micro.user.service.model.*;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Integer id;

    @NotNull(message = FieldErrorConstants.NOT_NULL)
    @NotBlank(message = FieldErrorConstants.NOT_BLANK)
    private String nameEnglish;

    @NotNull(message = FieldErrorConstants.NOT_NULL)
    @NotBlank(message = FieldErrorConstants.NOT_BLANK)
    private String nameNepali;

    @NotNull(message = FieldErrorConstants.NOT_NULL)
    @NotBlank(message = FieldErrorConstants.NOT_BLANK)
    private String email;

    @NotNull(message = FieldErrorConstants.NOT_NULL)
    @NotBlank(message = FieldErrorConstants.NOT_BLANK)
    private String about;

    @NotNull(message = FieldErrorConstants.NOT_NULL)
    private Integer identificationId;

    @NotNull(message = FieldErrorConstants.NOT_NULL)
    private Integer departmentId;

    @NotEmpty(message = FieldErrorConstants.NOT_EMPTY)
    private Set<Integer> roleIdList;

    @NotEmpty(message = FieldErrorConstants.NOT_EMPTY)
    private Set<Integer> addressIdList;

    public UserDto(User user) {
        this.id = user.getId();
        this.nameEnglish = user.getNameEnglish();
        this.nameNepali = user.getNameNepali();
        this.email = user.getEmail();
        this.about = user.getEmail();
    }
}

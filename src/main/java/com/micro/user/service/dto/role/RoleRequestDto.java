package com.micro.user.service.dto.role;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleRequestDto {

    private Integer id;

    private String nameEnglish;

    private String nameNepali;

    private String roleType;
}

package com.micro.user.service.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

@Getter
@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum AddressType {

    PERMANENT("PERMANENT", "स्थायी", "Permanent"),
    TEMPORARY("TEMPORARY", "अस्थायी", "Temporary");

    private final String value;
    private final String valueNepali;
    private final String valueEnglish;

    AddressType(String value, String valueNepali, String valueEnglish) {
        this.value = value;
        this.valueNepali = valueNepali;
        this.valueEnglish = valueEnglish;
    }

    public KeyValuePojo getEnum() {
        return KeyValuePojo.builder()
                .key(this.toString())
                .valueEnglish(this.valueEnglish)
                .valueNepali(this.valueNepali)
                .build();
    }
}
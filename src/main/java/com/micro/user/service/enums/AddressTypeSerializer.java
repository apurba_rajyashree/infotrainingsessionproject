package com.micro.user.service.enums;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class AddressTypeSerializer extends StdSerializer<Enum> {


    public AddressTypeSerializer() {
        this(null);
    }

    protected AddressTypeSerializer(Class<Enum> enumClass) {
        super(enumClass);
    }


    @Override
    public void serialize(Enum anEnum, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeObject(this.getSerializedData(anEnum));
    }

    private KeyValuePojo getSerializedData(Enum s) {
        return AddressType.valueOf(s.toString()).getEnum();
    }
}

package com.micro.user.service.enums;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class KeyValuePojo {

    private String key;

    private String valueEnglish;

    private String valueNepali;
}
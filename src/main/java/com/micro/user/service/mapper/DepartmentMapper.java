package com.micro.user.service.mapper;

import com.micro.user.service.dto.department.DepartmentResponseDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DepartmentMapper {
    List<DepartmentResponseDto> findAllDepartment();
}

package com.micro.user.service.practice;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Random;

@Getter
@Setter
@Slf4j
public class BinarySearchAlgorithm {

    Logger logger= LoggerFactory.getLogger(BinarySearchAlgorithm.class);

    private static final int FINDINGNUMBER = 78;
    HashMap<Integer, Boolean> eggMap = new HashMap<>();

    private Random rd = new Random();

    private static int findMiddle(int startingNumber, int endingNumber) {
        int sum = startingNumber + endingNumber;
        if (sum % 2 == 0) {
            return sum / 2;
        } else {
            return (sum - 1) / 2;
        }
    }

    public static int divideList(int startingNumber, int endingNumber, int middleNumber) {
        log.info("[" + startingNumber + "," + endingNumber + "]");
        if (middleNumber == FINDINGNUMBER) {
            return 1;
        }
        if (middleNumber > FINDINGNUMBER) {
            return 1 + divideList(startingNumber, middleNumber, findMiddle(startingNumber, middleNumber));
        } else {
            return 1 + divideList(middleNumber, endingNumber, findMiddle(middleNumber, endingNumber));
        }
    }

    /***
     * recurive method to find the egg break point using binary search algorithm
     * @param startingNumber
     * @param endingNumber
     * @param breakpoint
     * @return
     */
    public int eggBreakPoint(int startingNumber, int endingNumber, Integer breakpoint) {
        log.info("[" + startingNumber + "," + endingNumber + "]");

        int middleNumber;
        if (endingNumber - startingNumber <= 1) {
            return breakpoint;
        } else {
            middleNumber = findMiddle(startingNumber, endingNumber);

            if (Boolean.TRUE.equals(this.eggMap.get(middleNumber))) {
                breakpoint = middleNumber;
                return eggBreakPoint(startingNumber, middleNumber, breakpoint);
            } else {
                return eggBreakPoint(middleNumber, endingNumber, breakpoint);
            }
        }

    }

    /***
     * generate a random number and that will be our egg breakpoint and initialize hashmap according
     * @param startingNumber
     * @param endingNumber
     */
    public void eggMapping(int startingNumber, int endingNumber) {
        int randomNumber = rd.nextInt(endingNumber - startingNumber) + startingNumber;
        log.info(String.valueOf(randomNumber));
        for (int i = startingNumber; i <= endingNumber; i++) {
            if (i < randomNumber) {
                this.eggMap.put(i, false);
            } else {
                this.eggMap.put(i, true);
            }
        }
    }

    public static void main(String[] args) {
        int startingNumber = 1;
        int endingNumber = 1000;

        BinarySearchAlgorithm bsa = new BinarySearchAlgorithm();
        bsa.eggMapping(startingNumber, endingNumber);
        log.info("Breakpoint = " + bsa.eggBreakPoint(startingNumber, endingNumber, null));

        divideList(startingNumber, endingNumber, findMiddle(startingNumber, endingNumber));
    }
}

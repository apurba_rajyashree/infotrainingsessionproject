package com.micro.user.service.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "role")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_id_seq_gen")
    @SequenceGenerator(name = "role_id_seq_gen", sequenceName = "role_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "name_english", nullable = false, length = 40)
    private String nameEnglish;

    @Column(name = "name_nepali", nullable = false, length = 40)
    private String nameNepali;

    @Column(name = "role_type", nullable = false)
    private String roleType;

    @Column(name = "is_active")
    private boolean isActive;
}

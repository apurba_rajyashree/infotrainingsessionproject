package com.micro.user.service.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user", uniqueConstraints = {@UniqueConstraint(name = "uk_user_email", columnNames = "email")})
@Setter
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_seq_gen")
    @SequenceGenerator(name = "user_id_seq_gen", sequenceName = "user_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "name_english", length = 100, nullable = false)
    private String nameEnglish;
    @Column(name = "name_nepali", length = 100, nullable = false)
    private String nameNepali;

    @Column(name = "email", length = 100, nullable = false)
    private String email;

    @Column(name = "about")
    private String about;

    @OneToOne(fetch = FetchType.LAZY, targetEntity = Identification.class)
    @JoinColumn(name = "identification_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_user_identification"))
    private Identification identification;


    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Department.class)
    @JoinColumn(name = "department_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_user_department"))
    private Department department;

    @ManyToMany(cascade = {CascadeType.REMOVE})
    @JoinTable(
            name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    private List<Role> roleList = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    private List<Address> addressList;

}

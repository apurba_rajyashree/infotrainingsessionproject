package com.micro.user.service.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "department")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "department_id_seq_gen")
    @SequenceGenerator(name = "department_id_seq_gen", sequenceName = "department_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "name_english", nullable = false, length = 40)
    private String nameEnglish;

    @Column(name = "name_nepali", nullable = false, length = 40)
    private String nameNepali;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "is_active")
    private boolean isActive;
}

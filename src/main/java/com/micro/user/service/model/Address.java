package com.micro.user.service.model;

import com.micro.user.service.enums.AddressType;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "address")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_id_seq_gen")
    @SequenceGenerator(name = "address_id_seq_gen", sequenceName = "address_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "country", nullable = false, length = 40)
    private String country;

    @Column(name = "district", nullable = false, length = 40)
    private String district;

    @Column(name = "province", nullable = false, length = 40)
    private String province;

    @Column(name = "street", nullable = false)
    private String street;

    @Enumerated(EnumType.STRING)
    @Column(name = "address_type", nullable = false)
    private AddressType addressType;

    @Column(name = "is_active")
    private boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name = "user_id", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_address_user"))
    private User user;

}

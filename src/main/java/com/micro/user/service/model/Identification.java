package com.micro.user.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@Table(name = "identification")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Identification {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "identification_id_seq_gen")
    @SequenceGenerator(name = "identification_id_seq_gen", sequenceName = "identification_id_seq", allocationSize = 1)
    private Integer id;

    @Column(name = "identification_documeny_type", nullable = false, length = 40)
    private String identificationDocumentType;

    @Column(name = "identification_document_number", nullable = false)
    private String identificationDocumentNumber;

    @Column(name = "identification_document_issued_from", nullable = false)
    private String identificationDocumentIssuedFrom;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "identification_document_issued_date_ad", nullable = false)
    private Date identificationDocumentIssuedDateAd;

    @Column(name = "identification_document_issued_date_bs", nullable = false)
    private String identificationDocumentIssuedDateBs;
}

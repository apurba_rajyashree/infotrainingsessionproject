package com.micro.user.service.controller;

import com.micro.user.service.dto.DocumentResponse;
import com.micro.user.service.exeptions.ResourceNotFoundException;
import com.micro.user.service.model.Document;
import com.micro.user.service.repo.DocumentRepo;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FIleServiceImpl implements FileService {

    private final ModelMapper modelMapper;
    private final DocumentRepo documentRepo;


    @Override
    public String uploadImage(String path, MultipartFile file) throws IOException {

        //File Name
        String name = file.getOriginalFilename();

        String randomId = UUID.randomUUID().toString();
        String newFileName = randomId.concat(name.substring(name.lastIndexOf('.')));


        //Full Path
        // String filePath = path + File.separator + name;
        String filePath = path + File.separator + newFileName;


        //create folder if not created

        File oldFile = new File(path);
        if (!oldFile.exists()) {
            oldFile.mkdir();
        }

        //upload file
        Files.copy(file.getInputStream(), Paths.get(filePath));

        return newFileName;
    }

    @Override
    public String saveMultipartFile(MultipartFile multipartFile) throws IOException {
        String dirPath = System.getProperty("user.home") + "/" + "Desktop" + "SHOP_FILE_STORE";
        File directoryFile = new File(dirPath);
        if (!directoryFile.exists()) {
            directoryFile.mkdir();
        }
        String originalFileName = multipartFile.getOriginalFilename();
        String filePath = dirPath + "/" + originalFileName;
        File myFile = new File(filePath);
        try {
            multipartFile.transferTo(myFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Document document = Document.builder()
                .filePath(newPath)
                .uuid(newFileName)
                .originalName(originalFileName)
                .build();
        documentRepo.save(document);

        return filePath;
    }

    @Override
    public void downloadFile(String fileName, HttpServletResponse response) throws FileNotFoundException {
        Document document = documentRepo.findByUuid(fileName);
        DocumentResponse documentResponse = modelMapper.map(document, DocumentResponse.class);
        download(documentResponse, response);
    }


    public void download(DocumentResponse documentResponse, HttpServletResponse response) throws FileNotFoundException {
        if (documentResponse == null) throw new ResourceNotFoundException("Document Not Found");
        File file = new File(documentResponse.getFilePath());
        if (file.exists()) {
            String mimeType = URLConnection.guessContentTypeFromName(file.getName());
            if (mimeType == null) mimeType = "application/octet-stream";
            response.setContentType("application/octet-stream");
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + documentResponse.getOriginalName() + "\"");
            response.setContentLength((int) file.length());
            try (InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
                FileCopyUtils.copy(inputStream, response.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            throw new FileNotFoundException("File not found in server");
        }
    }
}

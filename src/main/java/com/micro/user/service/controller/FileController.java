package com.micro.user.service.controller;

import com.micro.user.service.utils.GlobalAPIResponse;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/file")
public class FileController {

    private final FileService fileService;

    @Value("${project.image}")
    private String path;

    @PostMapping("/upload")
    public ResponseEntity<GlobalAPIResponse> fileUpload(@RequestParam("image") MultipartFile image) {
        String fileName = null;
        try {
            fileName = fileService.uploadImage(path, image);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(new GlobalAPIResponse(false, "Image is not uploaded due to error on server !!", null), HttpStatusCode.valueOf(500));
        }
        return ResponseEntity.ok(new GlobalAPIResponse(true, "File uploaded successfully", fileName));

    }

    @PostMapping("/upload/file")
    public ResponseEntity<GlobalAPIResponse> multiPartFileUpload(@RequestParam("image") MultipartFile image) {
        String filePath = null;
        try {
            filePath = fileService.saveMultipartFile(image);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(new GlobalAPIResponse(false, "Image is not uploaded due to error on server !!", null), HttpStatusCode.valueOf(500));
        }
        return ResponseEntity.ok(new GlobalAPIResponse(true, "File uploaded successfully", filePath));

    }



    @GetMapping("/download")
    public void viewTempDocument(@RequestParam String fileName, HttpServletResponse response) throws FileNotFoundException {
        fileService.downloadFile(fileName, response);
    }
}

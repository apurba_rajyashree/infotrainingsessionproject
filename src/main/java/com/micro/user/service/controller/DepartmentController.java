package com.micro.user.service.controller;

import com.micro.user.service.constants.UserConstants;
import com.micro.user.service.dto.department.DepartmentRequestDto;
import com.micro.user.service.service.DepartmentService;
import com.micro.user.service.utils.GlobalAPIResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/departments")
@RequiredArgsConstructor
public class DepartmentController extends BaseController {
    private final DepartmentService departmentService;


    @PostMapping
    public ResponseEntity<GlobalAPIResponse> createDepartment(@Valid @RequestBody DepartmentRequestDto departmentRequestDto) {
        return ResponseEntity.ok(successCreate(UserConstants.DEPARTMENT, null));
    }

    @PutMapping
    public ResponseEntity<GlobalAPIResponse> updateDepartment(@Valid @RequestBody DepartmentRequestDto departmentRequestDto) {
        departmentService.update(departmentRequestDto);
        return ResponseEntity.ok(successUpdate(UserConstants.DEPARTMENT, null));
    }

    @GetMapping
    public ResponseEntity<GlobalAPIResponse> findAllDepartments() {
        return ResponseEntity.ok(successFetchList(UserConstants.DEPARTMENT, departmentService.findAll()));
    }

    @DeleteMapping("/{departmentId}")
    public ResponseEntity<GlobalAPIResponse> deleteUser(@PathVariable("departmentId") Integer departmentId) {
        departmentService.delete(departmentId);
        return ResponseEntity.ok(successDelete(UserConstants.DEPARTMENT, null));
    }
}

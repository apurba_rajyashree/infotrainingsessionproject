package com.micro.user.service.controller;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileService {

    String uploadImage(String path, MultipartFile file) throws IOException;
    String saveMultipartFile(MultipartFile file) throws IOException;
    void downloadFile(String fileName, HttpServletResponse response) throws FileNotFoundException;

}

package com.micro.user.service.controller;

import com.micro.user.service.constants.UserConstants;
import com.micro.user.service.dto.UserDto;
import com.micro.user.service.service.UserService;
import com.micro.user.service.utils.GlobalAPIResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController extends BaseController {

    public final UserService userService;

    @PostMapping
    public ResponseEntity<GlobalAPIResponse> createUser(@Valid @RequestBody UserDto userDto) {
        return ResponseEntity.ok(successCreate(UserConstants.USER, userService.saveUser(userDto)));
    }

    @PutMapping
    public ResponseEntity<GlobalAPIResponse> updateUser(@Valid @RequestBody UserDto userDto) {
        userService.updateUser(userDto);
        return ResponseEntity.ok(successUpdate(UserConstants.USER,null));
    }

    @GetMapping
    public ResponseEntity<GlobalAPIResponse> getAllUsers() {
        return ResponseEntity.ok(successFetchList(UserConstants.USER, userService.getAllUsers()));
    }

    @GetMapping("/{userId}")
    public ResponseEntity<GlobalAPIResponse> getUserById(@PathVariable("userId") Integer userId) {
        return ResponseEntity.ok(successFetch(UserConstants.USER, userService.getById(userId)));
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<GlobalAPIResponse> deleteUser(@PathVariable("userId") Integer userId) {
        userService.deleteUser(userId);
        return ResponseEntity.ok(successDelete(UserConstants.USER, null));
    }
}

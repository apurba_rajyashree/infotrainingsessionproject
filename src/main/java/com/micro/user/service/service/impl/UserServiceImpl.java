package com.micro.user.service.service.impl;

import com.micro.user.service.constants.ErrorConstants;
import com.micro.user.service.constants.UserConstants;
import com.micro.user.service.dto.UserDto;
import com.micro.user.service.dto.UserResponseDto;
import com.micro.user.service.exeptions.ResourceNotFoundException;
import com.micro.user.service.model.*;
import com.micro.user.service.repo.*;
import com.micro.user.service.service.UserService;
import com.micro.user.service.utils.CustomMessageSource;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final CustomMessageSource customMessageSource;
    private final DepartmentRepo departmentRepo;
    private final RoleRepo roleRepo;
    private final AddressRepo addressRepo;
    private final IdentificationRepo identificationRepo;

    private final ModelMapper modelMapper;

    public UserServiceImpl(UserRepo userRepo, CustomMessageSource customMessageSource, DepartmentRepo departmentRepo, RoleRepo roleRepo, AddressRepo addressRepo, IdentificationRepo identificationRepo, ModelMapper modelMapper) {
        this.userRepo = userRepo;
        this.customMessageSource = customMessageSource;
        this.departmentRepo = departmentRepo;
        this.roleRepo = roleRepo;
        this.addressRepo = addressRepo;
        this.identificationRepo = identificationRepo;
        this.modelMapper = modelMapper;
    }

    @Override
    public Integer saveUser(UserDto userDto) {
        log.info("inside save in UserServiceImpl");

        Department department = getDepartment(userDto);
        Identification identification = getIdentification(userDto);
        List<Role> roleList = getRoleList(userDto.getRoleIdList());
        List<Address> addressList = getAddressList(userDto.getAddressIdList());

        User user = User.builder()
                .nameEnglish(userDto.getNameEnglish())
                .nameNepali(userDto.getNameNepali())
                .email(userDto.getEmail())
                .about(userDto.getAbout())
                .department(department)
                .identification(identification)
                .roleList(roleList)
                .addressList(addressList)
                .build();

        log.info("before saving user in UserServiceImpl");
        user = userRepo.save(user);
        return user.getId();
    }

    private List<Role> getRoleList(Set<Integer> roleIdList) {
        List<Role> roleList = new ArrayList<>();
        for (Integer eachRoleId : roleIdList) {
            Role role = roleRepo.findById(eachRoleId).orElseThrow(
                    () -> new ResourceNotFoundException(customMessageSource.get(ErrorConstants.ERROR_DOESNOT_EXISTS,
                            customMessageSource.get(UserConstants.ROLE)))
            );
            roleList.add(role);
        }
        return roleList;
    }

    private List<Address> getAddressList(Set<Integer> addressIdList) {
        List<Address> addressList = new ArrayList<>();
        for (Integer eachAddressId : addressIdList) {
            Address address = addressRepo.findById(eachAddressId).orElseThrow(
                    () -> new ResourceNotFoundException(customMessageSource.get(ErrorConstants.ERROR_DOESNOT_EXISTS,
                            customMessageSource.get(UserConstants.ADDRESS)))
            );
            addressList.add(address);
        }
        return addressList;
    }

    @Override
    public void updateUser(UserDto userDto) {
        log.info("inside updateUser in UserServiceImpl");
        User user = getUser(userDto.getId());

        Department department = getDepartment(userDto);
        Identification identification = getIdentification(userDto);
        List<Role> roleList = getRoleList(userDto.getRoleIdList());
        List<Address> addressList = getAddressList(userDto.getAddressIdList());

        user = User.builder()
                .id(user.getId())
                .nameEnglish(userDto.getNameEnglish())
                .nameNepali(userDto.getNameNepali())
                .email(userDto.getEmail())
                .about(userDto.getAbout())
                .department(department)
                .identification(identification)
                .roleList(roleList)
                .addressList(addressList)
                .build();

        log.info("before saving user in UserServiceImpl");
        userRepo.save(user);
    }

    private Identification getIdentification(UserDto userDto) {
        return identificationRepo.findById(userDto.getIdentificationId()).orElseThrow(
                () -> new ResourceNotFoundException(customMessageSource.get(ErrorConstants.ERROR_DOESNOT_EXISTS,
                        customMessageSource.get(UserConstants.IDENTIFICATION)))
        );
    }

    private Department getDepartment(UserDto userDto) {
        return departmentRepo.findById(userDto.getDepartmentId()).orElseThrow(
                () -> new ResourceNotFoundException(customMessageSource.get(ErrorConstants.ERROR_DOESNOT_EXISTS,
                        customMessageSource.get(UserConstants.DEPARTMENT)))
        );
    }

    @Override
    public List<UserResponseDto> getAllUsers() {
        List<User> users = userRepo.findAll();
        return users.stream().map(UserResponseDto::new).collect(Collectors.toList());
    }

    @Override
    public UserDto getById(Integer userId) {
        User user = getUser(userId);
//        return new UserDto(user);
        UserDto userDto = modelMapper.map(user, UserDto.class);
        return userDto;
    }

    private User getUser(Integer userId) {
        return userRepo.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException(customMessageSource.get(ErrorConstants.ERROR_DOESNOT_EXISTS,
                        customMessageSource.get(UserConstants.USER)))
        );
    }

    @Transactional
    @Override
    public void deleteUser(Integer userId) {
        User user = getUser(userId);
        log.info("before deleting user in UserServiceImpl");
        userRepo.delete(user);
    }
}

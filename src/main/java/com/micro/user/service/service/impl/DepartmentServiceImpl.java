package com.micro.user.service.service.impl;

import com.micro.user.service.constants.ErrorConstants;
import com.micro.user.service.constants.UserConstants;
import com.micro.user.service.dto.department.DepartmentRequestDto;
import com.micro.user.service.dto.department.DepartmentResponseDto;
import com.micro.user.service.exeptions.ResourceNotFoundException;
import com.micro.user.service.model.Department;
import com.micro.user.service.repo.DepartmentRepo;
import com.micro.user.service.service.DepartmentService;
import com.micro.user.service.utils.CustomMessageSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepo departmentRepo;
    private final CustomMessageSource customMessageSource;

    @Override
    public void save(DepartmentRequestDto departmentRequestDto) {
        Department department = Department.builder()
                .nameEnglish(departmentRequestDto.getNameEnglish())
                .nameNepali(departmentRequestDto.getNameNepali())
                .description(departmentRequestDto.getDescription())
                .isActive(false)
                .build();
        departmentRepo.save(department);
    }

    @Override
    public void update(DepartmentRequestDto departmentRequestDto) {
        Department department = departmentRepo.findById(departmentRequestDto.getId()).orElseThrow(
                () -> new ResourceNotFoundException(customMessageSource.get(ErrorConstants.ERROR_DOESNOT_EXISTS,
                        customMessageSource.get(UserConstants.DEPARTMENT)))
        );

        department = Department.builder()
                .id(department.getId())
                .nameEnglish(departmentRequestDto.getNameEnglish())
                .nameNepali(departmentRequestDto.getNameNepali())
                .description(departmentRequestDto.getDescription())
                .isActive(false)
                .build();
        departmentRepo.save(department);
    }

    @Override
    public List<DepartmentResponseDto> findAll() {
        List<Department> departments = departmentRepo.findAll();
        return departments.stream().map(DepartmentResponseDto::new).toList();
    }

    @Override
    public void delete(Integer id) {
        Department department = departmentRepo.findById(id).orElseThrow(
                () -> new ResourceNotFoundException(customMessageSource.get(ErrorConstants.ERROR_DOESNOT_EXISTS,
                        customMessageSource.get(UserConstants.DEPARTMENT)))
        );
        department.setActive(false);
        departmentRepo.save(department);
    }
}

package com.micro.user.service.service;

import com.micro.user.service.dto.department.DepartmentRequestDto;
import com.micro.user.service.dto.department.DepartmentResponseDto;

import java.util.List;

public interface DepartmentService {

    void save(DepartmentRequestDto departmentRequestDto);
    void update(DepartmentRequestDto departmentRequestDto);
    List<DepartmentResponseDto> findAll();

    void delete(Integer id);
}

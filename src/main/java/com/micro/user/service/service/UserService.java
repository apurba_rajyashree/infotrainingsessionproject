package com.micro.user.service.service;


import com.micro.user.service.dto.UserDto;
import com.micro.user.service.dto.UserResponseDto;

import java.util.List;

public interface UserService {

    Integer saveUser(UserDto userDto);

    void updateUser(UserDto userDto);

    List<UserResponseDto> getAllUsers();

    UserDto getById(Integer userId);

    void deleteUser(Integer userId);
}

package com.micro.user.service.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GlobalAPIResponse {

    private Boolean status;
    private String message;
    private Object data;
}

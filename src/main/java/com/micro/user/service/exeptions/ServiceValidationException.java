package com.micro.user.service.exeptions;

public class ServiceValidationException extends RuntimeException{
    public ServiceValidationException(String message) {
        super(message);
    }
}
